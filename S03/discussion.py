# Code for Year
while True:
    try:
        num = int(input("Enter a Year: "))
        if num > 0:
            if num % 4 == 0:
                print(f'{num} is a leap year')
            else:
                print(f'{num} is not a leap year')
            break
        else:
            print("Value should be greater than 0")
    except ValueError:
        print("Please input integers only...")  
        continue

# Code for Grid of Asterisk
row = range(int(input("Enter Row: ")))
col = range(int(input("Enter Col: ")))

for i in row:
    for j in col:
        print("*", end=' ')
    print("\n")