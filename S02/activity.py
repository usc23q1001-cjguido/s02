name, age, occupation, movie, rating = "Celso Guido Jr", 22, "Student", "Puss n Boots 2", 95.99

print(f"I am {name}, and I am  {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}")

num1, num2, num3 = 10, 20, 50

print(num1 * num2)
print(num1 < num3)
print(num3 + num2)